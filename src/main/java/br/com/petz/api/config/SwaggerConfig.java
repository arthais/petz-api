package br.com.petz.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import({ BeanValidatorPluginsConfiguration.class })
public class SwaggerConfig {

	// info.app.properties's injected variables

	@Value("${info.app.name}")
	private String infoAppName;

	@Value("${info.app.description}")
	private String infoAppDescription;

	@Value("${info.app.version}")
	private String infoAppVersion;

	@Value("${info.app.license}")
	private String infoAppLicense;

	@Value("${info.app.licenseUrl}")
	private String infoAppLicenseUrl;

	@Value("${info.app.termsOfServiceUrl}")
	private String infoAppTermsOfServiceUrl;

	@Value("${info.app.contact.name}")
	private String infoAppContactName;

	@Value("${info.app.contact.url}")
	private String infoAppContactUrl;

	@Value("${info.app.contact.email}")
	private String infoAppContactEmail;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(getApiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.petz.api.controllers"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo getApiInfo() {
		Contact contact = new Contact(
				infoAppContactName,
				infoAppContactUrl,
				infoAppContactEmail);

		return new ApiInfoBuilder()
				.title(this.infoAppName)
				.description(this.infoAppDescription)
				.version(this.infoAppVersion)
				.license(this.infoAppLicense)
				.licenseUrl(this.infoAppLicenseUrl)
				.termsOfServiceUrl(this.infoAppTermsOfServiceUrl)
				.contact(contact)
				.build();
	}

}
