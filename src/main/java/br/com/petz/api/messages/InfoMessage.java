package br.com.petz.api.messages;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = APIMessages.theInfoMessage)
@Data
@EqualsAndHashCode(callSuper = true)
public class InfoMessage extends GenericMessage {

	private static final long serialVersionUID = -5852443269130358487L;

	public InfoMessage(@NotNull(message = APIMessages.itCannotBeNullOrEmpty) String message) {
		super(message, MessageType.INFO);
	}

}
