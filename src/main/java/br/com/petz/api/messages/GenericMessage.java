package br.com.petz.api.messages;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@ApiModel(description = APIMessages.theAncestralClassFromTheFourTypesOfMessagesThatCanBeReturnedWhetherSuccessInfoWarningOrError)
@Data
@AllArgsConstructor
public class GenericMessage implements Serializable {

	private static final long serialVersionUID = -7516443136807448635L;

	@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty)
	@ApiModelProperty(notes = APIMessages.theTextOfMessage, required = true)
	private String message;

	@NotNull(message = APIMessages.itCannotBeNull)
	@ApiModelProperty(notes = APIMessages.theMessageTypeWheterSuccessInfoWarningOrError, required = true) // , accessMode = AccessMode.READ_ONLY)
	private MessageType messageType;

}
