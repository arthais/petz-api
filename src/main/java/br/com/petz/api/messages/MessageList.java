package br.com.petz.api.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = APIMessages.aListOfMessagesWhichCanContainMessagesOfTheFourTypesSimultaneously)
@Data
public class MessageList implements Serializable {

	private static final long serialVersionUID = -8333371001118805361L;

	@NotNull(message = APIMessages.itCannotBeNull)
	@ApiModelProperty(notes = APIMessages.theListOfMessagesToBeReturnedToTheClient, required = true)
	private List<GenericMessage> messages;

	public MessageList(@NotNull(message = APIMessages.itCannotBeNull) List<GenericMessage> messages) {
		super();
		this.messages = messages;
	}

	public MessageList() {
		super();
		this.messages = new ArrayList<>();
	}

}
