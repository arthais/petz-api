package br.com.petz.api.messages;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = APIMessages.theWarningMessage)
@Data
@EqualsAndHashCode(callSuper = true)
public class WarningMessage extends GenericMessage {

	private static final long serialVersionUID = -5852443269130358487L;

	public WarningMessage(@NotNull(message = APIMessages.itCannotBeNullOrEmpty) String message) {
		super(message, MessageType.WARNING);
	}

}
