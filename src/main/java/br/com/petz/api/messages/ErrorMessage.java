package br.com.petz.api.messages;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = APIMessages.theErrorMessage)
@Data
@EqualsAndHashCode(callSuper = true)
public class ErrorMessage extends GenericMessage {

	private static final long serialVersionUID = -5852443269130358487L;

	public ErrorMessage(@NotNull(message = APIMessages.itCannotBeNullOrEmpty) String message) {
		super(message, MessageType.ERROR);
	}

}
