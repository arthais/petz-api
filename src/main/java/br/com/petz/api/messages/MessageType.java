package br.com.petz.api.messages;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = APIMessages.theTypesOfMessagesThatCanBeReturned)
public enum MessageType {

	@ApiModelProperty(notes = APIMessages.aMessageIndicatingThatEverythingHasHappenedSuccessfully)
	SUCCESS,
	@ApiModelProperty(notes = APIMessages.aMessageIndicatingThatItIsAnInformationToTheUser)
	INFO,
	@ApiModelProperty(notes = APIMessages.aMessageIndicatingAnImportantWarningToTheUser)
	WARNING,
	@ApiModelProperty(notes = APIMessages.aMessageIndicatingThatAnUnexpectedErrorOccurredDuringTheProcess)
	ERROR
}
