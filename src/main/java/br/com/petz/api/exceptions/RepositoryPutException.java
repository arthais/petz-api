package br.com.petz.api.exceptions;

public class RepositoryPutException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public RepositoryPutException(String message) {
		super(message);
	}

	public RepositoryPutException(String message, Throwable cause) {
		super(message, cause);
	}

}
