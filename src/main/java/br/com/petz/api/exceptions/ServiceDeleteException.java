package br.com.petz.api.exceptions;

public class ServiceDeleteException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ServiceDeleteException(String message) {
		super(message);
	}

	public ServiceDeleteException(String message, Throwable cause) {
		super(message, cause);
	}

}
