package br.com.petz.api.exceptions;

public class ConnectionException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ConnectionException(String message) {
		super(message);
	}

	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
