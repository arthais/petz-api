package br.com.petz.api.exceptions;

public class ServicePutException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ServicePutException(String message) {
		super(message);
	}

	public ServicePutException(String message, Throwable cause) {
		super(message, cause);
	}

}
