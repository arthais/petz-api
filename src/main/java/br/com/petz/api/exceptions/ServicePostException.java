package br.com.petz.api.exceptions;

public class ServicePostException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ServicePostException(String message) {
		super(message);
	}

	public ServicePostException(String message, Throwable cause) {
		super(message, cause);
	}

}
