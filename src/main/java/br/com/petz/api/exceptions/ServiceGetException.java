package br.com.petz.api.exceptions;

public class ServiceGetException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ServiceGetException(String message) {
		super(message);
	}

	public ServiceGetException(String message, Throwable cause) {
		super(message, cause);
	}

}
