package br.com.petz.api.exceptions;

public class RepositoryDeleteException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public RepositoryDeleteException(String message) {
		super(message);
	}

	public RepositoryDeleteException(String message, Throwable cause) {
		super(message, cause);
	}

}
