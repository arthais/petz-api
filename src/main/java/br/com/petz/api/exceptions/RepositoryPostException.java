package br.com.petz.api.exceptions;

public class RepositoryPostException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public RepositoryPostException(String message) {
		super(message);
	}

	public RepositoryPostException(String message, Throwable cause) {
		super(message, cause);
	}

}
