package br.com.petz.api.exceptions;

public class RepositoryGetException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public RepositoryGetException(String message) {
		super(message);
	}

	public RepositoryGetException(String message, Throwable cause) {
		super(message, cause);
	}

}
