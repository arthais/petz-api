package br.com.petz.api.exceptions;

public class ServiceValidationException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public ServiceValidationException(String message) {
		super(message);
	}

	public ServiceValidationException(String message, Throwable cause) {
		super(message, cause);
	}

}
