package br.com.petz.api.exceptions;

public class RepositoryNotFoundException extends Exception {

	private static final long serialVersionUID = 5692721501680663349L;

	public RepositoryNotFoundException(String message) {
		super(message);
	}

	public RepositoryNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
