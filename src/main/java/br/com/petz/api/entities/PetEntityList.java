package br.com.petz.api.entities;

import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = APIMessages.representsTheDataOfAnObjectThatContainsAListOfEntitiesOfTheClassPetEntity)
@Data
@EqualsAndHashCode(callSuper = true)
public class PetEntityList extends AbstractEntityList<PetEntity> {

	private static final long serialVersionUID = -4337033864042561226L;

	public PetEntityList() {
		super(PetEntity.class.getName());
	}

	public PetEntityList(@NotNull(message = APIMessages.itCannotBeNull) List<PetEntity> entityList) {
		super(PetEntity.class.getName(), entityList);
	}

}
