package br.com.petz.api.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ApiModel(description = APIMessages.representsAnCustomerSData)
@Entity(name = "Customer") // This name will be used to name this entity in the JPQL and HQL queries.
@Table(name = "customer") // This name will be used to name a table in DB.
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomerEntity extends AbstractEntity {

	private static final long serialVersionUID = 908689142346775924L;

	@Column(name = "customername", nullable = false, length = 60)
	@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty)
	@Size(min = 1, max = 60, message = APIMessages.itCannotBeLessThanMinOrMoreThanMaxCharacters)
	@ApiModelProperty(notes = APIMessages.TheCustomerLegalName, required = true)
	private String customerName;

	public CustomerEntity() {
		super();
	}

	public CustomerEntity(@DecimalMin(value = "1.0", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999.0", message = APIMessages.itCannotBeGreaterThan) BigDecimal id) {
		super(id);
	}

}
