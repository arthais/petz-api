package br.com.petz.api.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = APIMessages.theAncestralClassOfAnyEntityReturnedInResponseToARequestMadeByTheClient)
@MappedSuperclass
@Data
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

	// if there is any common code for all entities, put it here.

	private static final long serialVersionUID = 1555076986903149219L;

	@Id
	@Column(name = "id", nullable = false, precision = 28, scale = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@DecimalMin(value = "1.0", message = APIMessages.itCannotBeLessThan)
	@DecimalMax(value = "9999999999999999999999999999.0", message = APIMessages.itCannotBeGreaterThan)
	@ApiModelProperty(notes = APIMessages.theRecordId, required = false)
	public BigDecimal id;

	protected AbstractEntity(@DecimalMin(value = "1.0", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999.0", message = APIMessages.itCannotBeGreaterThan) BigDecimal id) {
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "AbstractEntity [" + "id=" + (id != null ? id.toString() : "null") + "]";
	}

}