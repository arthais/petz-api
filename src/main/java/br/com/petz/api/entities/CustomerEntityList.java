package br.com.petz.api.entities;

import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = APIMessages.representsTheDataOfAnObjectThatContainsAListOfEntitiesOfTheClassCustomerEntity)
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerEntityList extends AbstractEntityList<CustomerEntity> {

	private static final long serialVersionUID = -7079336897584087L;

	public CustomerEntityList() {
		super(CustomerEntity.class.getName());
	}

	public CustomerEntityList(@NotNull(message = APIMessages.itCannotBeNull) List<CustomerEntity> entityList) {
		super(CustomerEntity.class.getName(), entityList);
	}

}
