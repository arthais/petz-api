package br.com.petz.api.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ApiModel(description = APIMessages.representsTheDataOfAPetThatBelongsToAnCustomer)
@Entity(name = "Pet") // This name will be used to name this entity in the JPQL and HQL queries.
@Table(name = "pet") // This name will be used to name a table in DB.
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PetEntity extends AbstractEntity {

	private static final long serialVersionUID = -8482242040886089649L;

	@Column(name = "petname", nullable = false, length = 60)
	@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty)
	@Size(min = 1, max = 60, message = APIMessages.itCannotBeLessThanMinOrMoreThanMaxCharacters)
	@ApiModelProperty(notes = APIMessages.thePetLabel, required = true)
	private String petName;

	@NotNull(message = APIMessages.itCannotBeNull)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "customer", nullable = false)
	@ApiModelProperty(notes = APIMessages.theCustomerToWhichThisRecordBelongsToCustomerSIdOnPetzCustomerTable, required = true)
	private CustomerEntity customer;

	public PetEntity() {
		super();
	}

	public PetEntity(@DecimalMin(value = "1.0", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999.0", message = APIMessages.itCannotBeGreaterThan) BigDecimal id) {
		super(id);
	}

}
