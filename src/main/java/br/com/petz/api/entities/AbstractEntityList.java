package br.com.petz.api.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = APIMessages.representsTheDataOfAnObjectThatContainsAListOfEntitiesOfAnyClass)
@Data
@NoArgsConstructor
public abstract class AbstractEntityList<T> implements Serializable {

	private static final long serialVersionUID = -5295575639250317546L;

	@NotEmpty(message = APIMessages.itCannotBeEmpty)
	@ApiModelProperty(notes = APIMessages.theClassNameOfTheEntitiesThatMakeUpTheList, required = true)
	private String entityClassName;

	@NotNull(message = APIMessages.itCannotBeNull)
	@ApiModelProperty(notes = APIMessages.theListContainingTheEntities, required = true)
	private List<T> entityList;

	public AbstractEntityList(@NotEmpty(message = APIMessages.itCannotBeEmpty) String entityClassName) {
		super();
		this.entityClassName = entityClassName;
		this.entityList = new ArrayList<>();
	}

	public AbstractEntityList(@NotEmpty(message = APIMessages.itCannotBeEmpty) String entityClassName, @NotNull(message = APIMessages.itCannotBeNull) List<T> entityList) {
		super();
		this.entityClassName = entityClassName;
		this.entityList = entityList;
	}

}
