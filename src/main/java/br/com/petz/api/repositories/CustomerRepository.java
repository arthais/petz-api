package br.com.petz.api.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.api.entities.CustomerEntity;

@Repository
public interface CustomerRepository extends CrudRepository<CustomerEntity, BigDecimal> {

	List<CustomerEntity> findByCustomerNameContaining(String customerName);

}
