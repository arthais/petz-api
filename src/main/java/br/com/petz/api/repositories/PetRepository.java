package br.com.petz.api.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.petz.api.entities.CustomerEntity;
import br.com.petz.api.entities.PetEntity;

@Repository
public interface PetRepository extends CrudRepository<PetEntity, BigDecimal> {

	List<PetEntity> findByPetNameContaining(String petName);

	List<PetEntity> findByCustomer(CustomerEntity customer);

	long countByCustomer(CustomerEntity customer);
}
