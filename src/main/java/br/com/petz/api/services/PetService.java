package br.com.petz.api.services;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.petz.api.entities.CustomerEntity;
import br.com.petz.api.entities.PetEntity;
import br.com.petz.api.entities.PetEntityList;
import br.com.petz.api.exceptions.RepositoryDeleteException;
import br.com.petz.api.exceptions.RepositoryGetException;
import br.com.petz.api.exceptions.RepositoryNotFoundException;
import br.com.petz.api.exceptions.RepositoryPostException;
import br.com.petz.api.exceptions.RepositoryPutException;
import br.com.petz.api.exceptions.ServiceDeleteException;
import br.com.petz.api.exceptions.ServiceGetException;
import br.com.petz.api.exceptions.ServicePostException;
import br.com.petz.api.exceptions.ServicePutException;
import br.com.petz.api.exceptions.ServiceValidationException;
import br.com.petz.api.repositories.CustomerRepository;
import br.com.petz.api.repositories.PetRepository;
import br.com.petz.api.structures.APIMessages;

@Service
public class PetService extends AbstractService<PetEntity> {

	private final String entityListName = PetEntityList.class.getName();
	private final String entityName = PetEntity.class.getName();

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private PetRepository repository;

	public PetService() {
		super();
		logger = LoggerFactory.getLogger(PetService.class);
	}

	@Override
	public PetRepository getRepository() {
		return repository;
	}

	@Override
	public PetEntity post(PetEntity petEntity) throws RepositoryPostException, ServiceValidationException, ServicePostException {
		logger.info(MessageFormat.format(APIMessages.startingToPostTheResourceParameter + " - " + petEntity.toString(), entityName));

		try {
			petEntity = super.post(petEntity);
		} catch (RepositoryPostException | ServicePostException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBePostedParameter + " - " + e.getMessage() + " - " + petEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServicePostException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyPostedParameter + " - " + petEntity.toString(), entityName));

		return petEntity;
	}

	@Override
	public List<PetEntity> getAll() throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourcesParameter, entityListName));

		List<PetEntity> petEntityList;
		try {
			petEntityList = super.getAll();
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityListName);
			throw e;
		} catch (RepositoryGetException | ServiceGetException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourcesCouldNotBeRecoveredParameter + " - " + e.getMessage(), entityListName);
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourcesSuccessfullyRecoveredParameter, entityListName));

		return petEntityList;
	}

	public PetEntity getById(BigDecimal id) throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourceParameter + " - id: " + id.toString(), entityName));

		PetEntity petEntity;
		try {
			petEntity = super.getById(id);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryGetException | ServiceGetException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeRecoveredParameter + " id: " + id.toString() + " - " + e.getMessage(), entityName);
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyRecoveredParameter + " " + petEntity.toString(), entityName));

		return petEntity;
	}

	@Override
	public PetEntity put(PetEntity petEntity) throws RepositoryNotFoundException, RepositoryPutException, ServiceValidationException, ServicePutException {
		logger.info(MessageFormat.format(APIMessages.startingToUpdateTheResourceParameter, entityName));

		Optional<PetEntity> petOptionalTemp = repository.findById(petEntity.getId());
		if (!petOptionalTemp.isPresent()) {
			throw new ServiceValidationException(MessageFormat.format(APIMessages.theResourceWasNotFoundX, "petEntity.id = " + petEntity.getId().toString()));
		}

		if (petEntity.getCustomer() == null) {
			throw new ServiceValidationException(MessageFormat.format(APIMessages.theValueOfXPropertyCannotBeY, "petEntity.customer", "null"));
		} else {
			if (petEntity.getCustomer().getId() == null) {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.theValueOfXPropertyMustBeY, "petEntity.customer.id", "]1..*["));
			}
			Optional<CustomerEntity> customerOptionalTemp = customerRepository.findById(petEntity.getCustomer().getId());
			if (customerOptionalTemp.isPresent()) {
				petEntity.setCustomer(customerOptionalTemp.get());
			} else {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.theResourceWasNotFoundX, "petEntity.customer.id = " + petEntity.getCustomer().getId().toString()));
			}
		}

		try {
			petEntity = super.put(petEntity);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryPutException | ServicePutException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeUpdatedParameter + " - " + e.getMessage() + " - " + petEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServicePutException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyUpdatedParameter, entityName));

		return petEntity;
	}

	@Override
	public void delete(PetEntity petEntity) throws RepositoryNotFoundException, RepositoryDeleteException, ServiceValidationException, ServiceDeleteException {
		logger.info(MessageFormat.format(APIMessages.startingToDeleteTheResourceParameter + " - id: " + petEntity.getId().toString(), entityName));

		try {
			super.delete(petEntity);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryDeleteException | ServiceDeleteException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeDeletedParameter + " - " + e.getMessage() + " - " + petEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServiceDeleteException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyDeletedParameter + " - id: " + petEntity.getId().toString(), entityName));
	}

}
