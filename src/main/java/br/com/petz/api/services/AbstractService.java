package br.com.petz.api.services;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.springframework.data.repository.CrudRepository;

import br.com.petz.api.exceptions.RepositoryDeleteException;
import br.com.petz.api.exceptions.RepositoryGetException;
import br.com.petz.api.exceptions.RepositoryNotFoundException;
import br.com.petz.api.exceptions.RepositoryPostException;
import br.com.petz.api.exceptions.RepositoryPutException;
import br.com.petz.api.exceptions.ServiceDeleteException;
import br.com.petz.api.exceptions.ServiceGetException;
import br.com.petz.api.exceptions.ServicePostException;
import br.com.petz.api.exceptions.ServicePutException;
import br.com.petz.api.exceptions.ServiceValidationException;
import br.com.petz.api.structures.APIMessages;

public abstract class AbstractService<T> {

	// if there is any common code for all services, put it here.

	protected Logger logger;

	public abstract CrudRepository<T, BigDecimal> getRepository();

	public AbstractService() {
		super();
	}

	public T post(T entity) throws RepositoryPostException, ServiceValidationException, ServicePostException {
		try {
			Method method = entity.getClass().getMethod("getId");
			BigDecimal id = (BigDecimal) method.invoke(entity);

			if (id != null) {
				String message = APIMessages.theResourceCouldNotBePosted + " " + APIMessages.theResourceCannotHaveTheIdPropertyFilled + " " + entity.toString();
				Exception e = new ServicePostException(message);
				logger.error(message, e);
				throw e;
			}

			try {
				entity = getRepository().save(entity);
			} catch (Exception e) {
				Throwable eTemp = e;
				while (eTemp != null && !(eTemp instanceof ConstraintViolationException)) {
					eTemp = eTemp.getCause();
				}
				if (eTemp instanceof ConstraintViolationException) {
					String message = APIMessages.theResourceCouldNotBePosted;
					for (ConstraintViolation<?> constraintViolation : ((ConstraintViolationException) eTemp).getConstraintViolations()) {
						message += " " + constraintViolation.getRootBeanClass() + " -> " + constraintViolation.getPropertyPath() + " -> " + constraintViolation.getMessage();
						break;
					}
					message += " " + entity.toString();
					Exception e2 = new RepositoryPostException(message, eTemp);
					logger.warn(message, e2);
					throw e2;
				} else {
					String message = APIMessages.theResourceCouldNotBePosted + " " + e.getMessage() + " " + entity.toString();
					logger.error(message, e);
					throw new RepositoryPostException(message, e);
				}
			}

			return entity;
		} catch (RepositoryPostException | ServicePostException e) {
			throw e;
		} catch (Exception e) {
			String message = APIMessages.theResourceCouldNotBePosted + " " + e.getMessage() + " " + entity.toString();
			logger.error(message, e);
			throw new ServicePostException(message, e);
		}
	}

	public List<T> getAll() throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		try {
			Iterable<T> entitiesTemp;
			try {
				entitiesTemp = getRepository().findAll();
			} catch (Exception e) {
				String message = APIMessages.theResourcesCouldNotBeRecovered + " " + e.getMessage();
				logger.error(message, e);
				throw new RepositoryGetException(message, e);
			}

			List<T> entities = new ArrayList<T>();
			for (T entity : entitiesTemp) {
				entities.add(entity);
			}

			if (entities.isEmpty()) {
				String message = APIMessages.theResourcesCouldNotBeRecovered + " " + APIMessages.theResourcesWasNotFound;
				logger.warn(message);
				throw new RepositoryNotFoundException(message);
			}

			return entities;
		} catch (RepositoryGetException | RepositoryNotFoundException e) {
			throw e;
		} catch (Exception e) {
			String message = APIMessages.theResourcesCouldNotBeRecovered + " " + e.getMessage();
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}
	}

	public T getById(BigDecimal id) throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		try {
			Optional<T> optional;
			try {
				optional = getRepository().findById(id);
			} catch (Exception e) {
				String message = APIMessages.theResourceCouldNotBeRecovered + " id: " + id.toString() + " - " + e.getMessage();
				logger.error(message, e);
				throw new RepositoryGetException(message, e);
			}

			if (!optional.isPresent()) {
				String message = APIMessages.theResourceCouldNotBeRecovered + " " + APIMessages.theResourceWasNotFound + " id: " + id.toString();
				logger.warn(message);
				throw new RepositoryNotFoundException(message);
			}

			T entity = optional.get();

			return entity;
		} catch (RepositoryGetException | RepositoryNotFoundException e) {
			throw e;
		} catch (Exception e) {
			String message = APIMessages.theResourceCouldNotBeRecovered + " id: " + id.toString() + " - " + e.getMessage();
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}
	}

	public T put(T entity) throws RepositoryNotFoundException, RepositoryPutException, ServiceValidationException, ServicePutException {
		try {
			Method method = entity.getClass().getMethod("getId");
			BigDecimal id = (BigDecimal) method.invoke(entity);

			if (id == null) {
				String message = APIMessages.theResourceCouldNotBeUpdated + " " + APIMessages.theResourceMustHaveTheIdPropertyFilled + " " + entity.toString();
				Exception e = new ServicePutException(message);
				logger.error(message, e);
				throw e;
			}

			Optional<T> optional;
			try {
				optional = getRepository().findById(id);
			} catch (Exception e) {
				String message = APIMessages.theResourceCouldNotBeUpdated + " " + APIMessages.theExistenceOfTheResourceCannotBeVerified + " id: " + id.toString() + " - " + e.getMessage();
				logger.error(message, e);
				throw new RepositoryPutException(message, e);
			}

			if (!optional.isPresent()) {
				String message = APIMessages.theResourceCouldNotBeUpdated + " " + APIMessages.theResourceWasNotFound + " id: " + id.toString();
				logger.warn(message);
				throw new RepositoryNotFoundException(message);
			}

			try {
				entity = getRepository().save(entity);
			} catch (Exception e) {
				Throwable eTemp = e;
				while (eTemp != null && !(eTemp instanceof ConstraintViolationException)) {
					eTemp = eTemp.getCause();
				}
				if (eTemp instanceof ConstraintViolationException) {
					String message = APIMessages.theResourceCouldNotBeUpdated;
					for (ConstraintViolation<?> constraintViolation : ((ConstraintViolationException) eTemp).getConstraintViolations()) {
						message += " " + constraintViolation.getRootBeanClass() + " -> " + constraintViolation.getPropertyPath() + " -> " + constraintViolation.getMessage();
						break;
					}
					message += " " + entity.toString();
					Exception e2 = new RepositoryPutException(message, eTemp);
					logger.warn(message, e2);
					throw e2;
				} else {
					String message = APIMessages.theResourceCouldNotBeUpdated + " " + e.getMessage() + " " + entity.toString();
					logger.error(message, e);
					throw new RepositoryPutException(message, e);
				}
			}

			return entity;
		} catch (RepositoryNotFoundException | RepositoryPutException | ServicePutException e) {
			throw e;
		} catch (Exception e) {
			String message = APIMessages.theResourceCouldNotBeUpdated + " " + e.getMessage() + " " + entity.toString();
			logger.error(message, e);
			throw new ServicePutException(message, e);
		}
	}

	public void delete(T entity) throws RepositoryNotFoundException, RepositoryDeleteException, ServiceValidationException, ServiceDeleteException {
		try {
			Method method = entity.getClass().getMethod("getId");
			BigDecimal id = (BigDecimal) method.invoke(entity);

			if (id == null) {
				String message = APIMessages.theResourceCouldNotBeDeleted + " " + APIMessages.theResourceMustHaveTheIdPropertyFilled + " " + entity.toString();
				Exception e = new ServiceDeleteException(message);
				logger.error(message, e);
				throw e;
			}

			Optional<T> optional;
			try {
				optional = getRepository().findById(id);
			} catch (Exception e) {
				String message = APIMessages.theResourceCouldNotBeDeleted + " " + APIMessages.theExistenceOfTheResourceCannotBeVerified + " id: " + id.toString() + " - " + e.getMessage();
				logger.error(message, e);
				throw new RepositoryDeleteException(message, e);
			}

			if (!optional.isPresent()) {
				String message = APIMessages.theResourceCouldNotBeDeleted + " " + APIMessages.theResourceWasNotFound + " id: " + id.toString();
				logger.warn(message);
				throw new RepositoryNotFoundException(message);
			}

			try {
				getRepository().delete(entity);
			} catch (Exception e) {
				String message = APIMessages.theResourceCouldNotBeDeleted + " " + e.getMessage() + " " + entity.toString();
				logger.error(message, e);
				throw new RepositoryDeleteException(message, e);
			}
		} catch (RepositoryNotFoundException | RepositoryDeleteException | ServiceDeleteException e) {
			throw e;
		} catch (Exception e) {
			String message = APIMessages.theResourceCouldNotBeDeleted + " " + e.getMessage() + " " + entity.toString();
			logger.error(message, e);
			throw new ServiceDeleteException(message, e);
		}
	}

}
