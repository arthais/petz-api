package br.com.petz.api.services;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.petz.api.entities.CustomerEntity;
import br.com.petz.api.entities.CustomerEntityList;
import br.com.petz.api.entities.PetEntity;
import br.com.petz.api.exceptions.RepositoryDeleteException;
import br.com.petz.api.exceptions.RepositoryGetException;
import br.com.petz.api.exceptions.RepositoryNotFoundException;
import br.com.petz.api.exceptions.RepositoryPostException;
import br.com.petz.api.exceptions.RepositoryPutException;
import br.com.petz.api.exceptions.ServiceDeleteException;
import br.com.petz.api.exceptions.ServiceGetException;
import br.com.petz.api.exceptions.ServicePostException;
import br.com.petz.api.exceptions.ServicePutException;
import br.com.petz.api.exceptions.ServiceValidationException;
import br.com.petz.api.repositories.CustomerRepository;
import br.com.petz.api.repositories.PetRepository;
import br.com.petz.api.structures.APIMessages;

@Service
public class CustomerService extends AbstractService<CustomerEntity> {

	private final String entityListName = CustomerEntityList.class.getName();
	private final String entityName = CustomerEntity.class.getName();

	@Autowired
	private CustomerRepository repository;

	@Autowired
	private PetRepository petRepository;

	public CustomerService() {
		super();
		logger = LoggerFactory.getLogger(CustomerService.class);
	}

	@Override
	public CustomerRepository getRepository() {
		return repository;
	}

	@Override
	public CustomerEntity post(CustomerEntity customerEntity) throws RepositoryPostException, ServiceValidationException, ServicePostException {
		logger.info(MessageFormat.format(APIMessages.startingToPostTheResourceParameter + " - " + customerEntity.toString(), entityName));

		try {
			customerEntity = super.post(customerEntity);
		} catch (RepositoryPostException | ServicePostException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBePostedParameter + " - " + e.getMessage() + " - " + customerEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServicePostException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyPostedParameter + " - " + customerEntity.toString(), entityName));

		return customerEntity;
	}

	@Override
	public List<CustomerEntity> getAll() throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourcesParameter, entityListName));

		List<CustomerEntity> customerEntityList;
		try {
			customerEntityList = super.getAll();
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityListName);
			throw e;
		} catch (RepositoryGetException | ServiceGetException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourcesCouldNotBeRecoveredParameter + " - " + e.getMessage(), entityListName);
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourcesSuccessfullyRecoveredParameter, entityListName));

		return customerEntityList;
	}

	public CustomerEntity getById(BigDecimal id) throws RepositoryNotFoundException, RepositoryGetException, ServiceGetException {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourceParameter + " - id: " + id.toString(), entityName));

		CustomerEntity customerEntity;
		try {
			customerEntity = super.getById(id);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryGetException | ServiceGetException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeRecoveredParameter + " id: " + id.toString() + " - " + e.getMessage(), entityName);
			logger.error(message, e);
			throw new ServiceGetException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyRecoveredParameter + " " + customerEntity.toString(), entityName));

		return customerEntity;
	}

	@Override
	public CustomerEntity put(CustomerEntity customerEntity) throws RepositoryNotFoundException, RepositoryPutException, ServiceValidationException, ServicePutException {
		logger.info(MessageFormat.format(APIMessages.startingToUpdateTheResourceParameter, entityName));

		Optional<CustomerEntity> customerOptionalTemp = repository.findById(customerEntity.getId());
		if (!customerOptionalTemp.isPresent()) {
			throw new ServiceValidationException(MessageFormat.format(APIMessages.theResourceWasNotFoundX, "customerEntity.id = " + customerEntity.getId().toString()));
		}

		try {
			customerEntity = super.put(customerEntity);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryPutException | ServicePutException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeUpdatedParameter + " - " + e.getMessage() + " - " + customerEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServicePutException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyUpdatedParameter, entityName));

		return customerEntity;
	}

	@Override
	public void delete(CustomerEntity customerEntity) throws RepositoryNotFoundException, RepositoryDeleteException, ServiceValidationException, ServiceDeleteException {
		logger.info(MessageFormat.format(APIMessages.startingToDeleteTheResourceParameter + " - id: " + customerEntity.getId().toString(), entityName));

		long recordCount = 0;

		recordCount = petRepository.countByCustomer(customerEntity);
		if (recordCount > 0) {
			throw new ServiceValidationException(MessageFormat.format(APIMessages.thereAreDependentResourcesXInstancesOfY, recordCount, PetEntity.class.getSimpleName()));
		}

		try {
			super.delete(customerEntity);
		} catch (RepositoryNotFoundException e) {
			logger.warn(e.getMessage() + " " + entityName);
			throw e;
		} catch (RepositoryDeleteException | ServiceDeleteException e) {
			throw e;
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeDeletedParameter + " - " + e.getMessage() + " - " + customerEntity.toString(), entityName);
			logger.error(message, e);
			throw new ServiceDeleteException(message, e);
		}

		logger.info(MessageFormat.format(APIMessages.theResourceSuccessfullyDeletedParameter + " - id: " + customerEntity.getId().toString(), entityName));
	}

}
