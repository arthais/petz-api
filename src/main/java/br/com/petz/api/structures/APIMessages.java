package br.com.petz.api.structures;

public abstract class APIMessages {

	// used at documentation of the classes into messages package.
	public static final String aListOfMessagesWhichCanContainMessagesOfTheFourTypesSimultaneously = "A list of messages, which can contain messages of the four types simultaneously.";
	public static final String aMessageIndicatingAnImportantWarningToTheUser = "A message indicating an important warning to the user.";
	public static final String aMessageIndicatingThatAnUnexpectedErrorOccurredDuringTheProcess = "A message indicating that an unexpected error occurred during the process.";
	public static final String aMessageIndicatingThatEverythingHasHappenedSuccessfully = "A message indicating that everything has happened successfully.";
	public static final String aMessageIndicatingThatItIsAnInformationToTheUser = "A message indicating that it is an information to the user.";
	public static final String theAncestralClassFromTheFourTypesOfMessagesThatCanBeReturnedWhetherSuccessInfoWarningOrError = "The ancestral class from the four types of messages that can be returned: whether SUCCESS, INFO, WARNING or ERROR.";
	public static final String theErrorMessage = "A ERROR message.";
	public static final String theInfoMessage = "The INFO message.";
	public static final String theListOfMessagesToBeReturnedToTheClient = "The list of messages to be returned to the Client.";
	public static final String theMessageTypeWheterSuccessInfoWarningOrError = "The message type: wheter SUCCESS, INFO, WARNING or ERROR.";
	public static final String theSuccessMessage = "The SUCCESS message.";
	public static final String theTextOfMessage = "The text of message.";
	public static final String theTypesOfMessagesThatCanBeReturned = "The types of messages that can be returned.";
	public static final String theWarningMessage = "The WARNING message.";

	// used at documentation of the classes into structures package.

	// APIResponse
	public static final String theClassUsedToTransportTheResultOfARequestBodyToTheClient = "The class used to transport the result of a request (body) to the client.";
	public static final String theHttpStatusReturnedToTheClient = "The HTTP status returned to the client.";
	public static final String theObjectContainingTheListOfMessagesToBeReturnedToTheClientSuccessInformationWarningAndErrorMessages = "The object containing the list of messages to be returned to the client: success, information, warning and error messages.";
	public static final String theApiSStructuresOrEntitiesReturnedToTheClient = "The API's structure(s) or entity(ies) returned to the client.";
	public static final String theHttpStatusCodeReturnedToTheClient = "The HTTP status code returned to the client.";

	// used at documentation of the classes into the entities package.

	// general
	public static final String theCustomerToWhichThisRecordBelongsToCustomerSIdOnPetzCustomerTable = "The customer to which this record belongs to. Customer's ID on petz.customer table.";

	// AbstractEntity and AbstractEntityList
	public static final String theAncestralClassOfAnyEntityReturnedInResponseToARequestMadeByTheClient = "The ancestral class of any entity returned in response to a request made by the client.";
	public static final String theRecordId = "The record id.";
	public static final String representsTheDataOfAnObjectThatContainsAListOfEntitiesOfAnyClass = "Represents the data of an object that contains a list of entities of any class.";
	public static final String theClassNameOfTheEntitiesThatMakeUpTheList = "The class name of the entities that make up the list.";
	public static final String theListContainingTheEntities = "The list containing the entities.";

	// CustomerEntity and CustomerEntityList
	public static final String representsAnCustomerSData = "Represents an customer's data.";
	public static final String TheCustomerLegalName = "The customer legal name.";
	public static final String theCustomerFancyName = "The customer fancy name.";
	public static final String representsTheDataOfAnObjectThatContainsAListOfEntitiesOfTheClassCustomerEntity = "Represents the data of an object that contains a list of entities of the class CustomerEntity.";

	// PetEntity and PetEntityList
	public static final String representsTheDataOfAPetThatBelongsToAnCustomer = "Represents the data of a pet that belongs to an customer.";
	public static final String thePetLabel = "The pet label.";
	public static final String representsTheDataOfAnObjectThatContainsAListOfEntitiesOfTheClassPetEntity = "Represents the data of an object that contains a list of entities of the class PetEntity.";

	// used at validations of the properties and method parameters of entities and
	// other structures.
	public static final String itCannotBeEmpty = "It cannot be empty.";
	public static final String itCannotBeGreaterThan = "It cannot be greater than {value}.";
	public static final String itCannotBeLessThan = "It cannot be less than {value}.";
	public static final String itCannotBeLessThanMinOrMoreThanMaxCharacters = "It cannot be less than {min} or more than {max} characters.";
	public static final String itCannotBeNull = "It cannot be null.";
	public static final String itCannotBeNullOrEmpty = "It cannot be null or empty.";

	// used at messages of the classes into the repositories and services packages.

	public static final String theResourceCannotHaveTheIdPropertyFilled = "The resource cannot have the id property filled.";
	public static final String theResourceCouldNotBePosted = "The resource could not be posted.";
	public static final String theResourcesCouldNotBeRecovered = "The resources could not be recovered.";
	public static final String theResourcesWasNotFound = "The resources was not found.";
	public static final String theResourceCouldNotBeRecovered = "The resource could not be recovered.";
	public static final String theResourceWasNotFound = "The resource was not found.";
	public static final String theResourceMustHaveTheIdPropertyFilled = "The resource must have the id property filled.";
	public static final String theExistenceOfTheResourceCannotBeVerified = "The existence of the resource cannot be verified.";
	public static final String theResourceCouldNotBeUpdated = "The resource could not be updated.";
	public static final String theResourceCouldNotBeDeleted = "The resource could not be deleted.";

	// used at messages of the classes into the services package.

	// general
	public static final String theValueOfXPropertyMustBeY = "The value of {0} property must be {1}.";
	public static final String theValueOfXPropertyCannotBeY = "The value of {0} property cannot be {1}.";
	public static final String theResourceWasNotFoundX = "The resource was not found: {0}";
	public static final String thereAreDependentResourcesXInstancesOfY = "There are dependent resources: {0} instance(s) of {1}";
	public static final String thePathParamIdMustMatchTheEntityIdProperty = "The pathParam \"id = {0}\" must match the \"entity.id = {1}\" property.";

	// RESOURCES

	// used at messages of the classes into the services and resources packages.
	public static final String startingToPostTheResourceParameter = "Starting to post the resource: {0}";
	public static final String theResourceSuccessfullyPostedParameter = "The resource successfully posted: {0}";
	public static final String theResourceCouldNotBePostedParameter = "The resource could not be posted: {0}";
	public static final String startingToRecoveryTheResourcesParameter = "Starting to recovery the resources: {0}";
	public static final String theResourcesSuccessfullyRecoveredParameter = "The resources successfully recovered: {0}";
	public static final String theResourcesCouldNotBeRecoveredParameter = "The resources could not be recovered: {0}";
	public static final String startingToRecoveryTheResourceParameter = "Starting to recovery the resource: {0}";
	public static final String theResourceSuccessfullyRecoveredParameter = "The resource successfully recovered: {0}";
	public static final String theResourceCouldNotBeRecoveredParameter = "The resource could not be recovered: {0}";
	public static final String startingToUpdateTheResourceParameter = "Starting to update the resource: {0}";
	public static final String theResourceSuccessfullyUpdatedParameter = "The resource successfully updated: {0}";
	public static final String theResourceCouldNotBeUpdatedParameter = "The resource could not be updated: {0}";
	public static final String startingToDeleteTheResourceParameter = "Starting to delete the resource: {0}";
	public static final String theResourceSuccessfullyDeletedParameter = "The resource successfully deleted: {0}";
	public static final String theResourceCouldNotBeDeletedParameter = "The resource could not be deleted: {0}";

	// AbastractController
	public static final String therResourceSuccessfullyAccessed = "The resource successfully accessed.";
	public static final String theRequestAppearsToBePoorlyFormatted = "The request appears to be poorly formatted.";
	public static final String youAreNotAuthorizedToAccessTheResource = "You are not authorized to access the resource.";
	public static final String theResourceYouAreTryingToAccessIsProhibited = "The resource you are trying to access is prohibited.";
	public static final String theResourceYouWereTryingToAccessWasNotFound = "The resource you were trying to access was not found.";
	public static final String anUnexpectedSystemErrorHasOccurred = "An unexpected system error has occurred.";

	// CustomerController
	public static final String itRepresentsAllResourcesRelatedToTheCustomers = "It represents all resources related to the Customers.";
	public static final String itStoresACustomerInTheDatabase = "It stores a Customer in the database.";
	public static final String itProvidesAListOfAllCustomers = "It provides a list of all Customers.";
	public static final String itProvidesACustomerIdentifiedByTheGivenId = "It provides a Customer identified by the given ID.";
	public static final String itUpdatesACustomerInTheDatabase = "It updates a Customer in the database.";
	public static final String itDeletesACustomerInTheDatabase = "It deletes a Customer in the database.";

	// PetController
	public static final String itRepresentsAllResourcesRelatedToThePets = "It represents all resources related to the Pets.";
	public static final String itStoresAPetInTheDatabase = "It stores a Pet in the database.";
	public static final String itProvidesAListOfAllPets = "It provides a list of all Pets.";
	public static final String itProvidesAPetIdentifiedByTheGivenId = "It provides a Pet identified by the given ID.";
	public static final String itUpdatesAPetInTheDatabase = "It updates a Pet in the database.";
	public static final String itDeletesAPetInTheDatabase = "It deletes a Pet in the database.";

}
