package br.com.petz.api.structures;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.http.HttpStatus;

import br.com.petz.api.messages.GenericMessage;
import br.com.petz.api.messages.MessageList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = APIMessages.theClassUsedToTransportTheResultOfARequestBodyToTheClient)
@Data
@NoArgsConstructor
@XmlRootElement
public class APIResponse implements Serializable {

	private static final long serialVersionUID = 3447310892297418019L;

	@ApiModelProperty(notes = APIMessages.theHttpStatusReturnedToTheClient, required = true, position = 2)
	@NotNull(message = APIMessages.itCannotBeNull)
	private HttpStatus httpStatus;

	@ApiModelProperty(notes = APIMessages.theObjectContainingTheListOfMessagesToBeReturnedToTheClientSuccessInformationWarningAndErrorMessages, required = true, position = 3)
	@NotNull(message = APIMessages.itCannotBeNull)
	private MessageList messageList;

	@ApiModelProperty(notes = APIMessages.theApiSStructuresOrEntitiesReturnedToTheClient, required = false, position = 4)
	private Object model;

	public APIResponse(
			@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty) MessageList messageList,
			@NotNull(message = APIMessages.itCannotBeNull) HttpStatus httpStatus) {
		super();
		this.messageList = messageList;
		this.httpStatus = httpStatus;
		this.model = null;
	}

	public APIResponse(
			@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty) GenericMessage message,
			@NotNull(message = APIMessages.itCannotBeNull) HttpStatus httpStatus) {
		super();
		if (this.messageList == null) {
			this.messageList = new MessageList();
		}
		this.messageList.getMessages().add(message);
		this.httpStatus = httpStatus;
		this.model = null;
	}

	public APIResponse(
			@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty) MessageList messageList,
			@NotNull(message = APIMessages.itCannotBeNull) HttpStatus httpStatus,
			Object model) {
		super();
		this.messageList = messageList;
		this.httpStatus = httpStatus;
		this.model = model;
	}

	public APIResponse(
			@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty) GenericMessage message,
			@NotNull(message = APIMessages.itCannotBeNull) HttpStatus httpStatus,
			Object model) {
		super();
		if (this.messageList == null) {
			this.messageList = new MessageList();
		}
		this.messageList.getMessages().add(message);
		this.httpStatus = httpStatus;
		this.model = model;
	}

	@ApiModelProperty(notes = APIMessages.theHttpStatusCodeReturnedToTheClient, required = true, position = 1)
	public int getHttpCode() {
		return getHttpStatus().value();
	}

	public void addMessage(@NotEmpty(message = APIMessages.itCannotBeNullOrEmpty) GenericMessage message) {
		this.messageList.getMessages().add(message);
	}

	public void removeMessage(@Min(value = 0, message = APIMessages.itCannotBeLessThan) final int index) {
		if (index > this.messageList.getMessages().size() - 1) {
			throw new IllegalArgumentException(String.format(APIMessages.itCannotBeGreaterThan, this.messageList.getMessages().size() - 1));
		}
		this.messageList.getMessages().remove(index);
	}

	public void clearMessages() {
		this.messageList.getMessages().clear();
	}

}
