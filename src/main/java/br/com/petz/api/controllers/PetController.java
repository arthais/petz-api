package br.com.petz.api.controllers;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.api.entities.PetEntity;
import br.com.petz.api.entities.PetEntityList;
import br.com.petz.api.services.PetService;
import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/pets")
@Api(tags = { "Pets" }, description = APIMessages.itRepresentsAllResourcesRelatedToThePets)
public class PetController extends AbstractController<PetEntity> {

	Logger logger = LoggerFactory.getLogger(PetController.class);

	@Autowired
	private PetService service;

	@ApiOperation(value = APIMessages.itStoresAPetInTheDatabase, response = PetEntity.class)
	@RequestMapping(value = { "" }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(HttpServletRequest request, @ApiParam(value = "Pet entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) PetEntity entity) {
		return super.post(request, logger, service, entity);
	}

	@ApiOperation(value = APIMessages.itProvidesAListOfAllPets, response = PetEntityList.class)
	@RequestMapping(value = { "" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAll(HttpServletRequest request) {
		return super.getAll(request, logger, service, new PetEntityList());
	}

	@ApiOperation(value = APIMessages.itProvidesAPetIdentifiedByTheGivenId, response = PetEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getById(HttpServletRequest request, @ApiParam(value = "ID of pet entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id) {
		return super.getById(request, logger, service, id);
	}

	@ApiOperation(value = APIMessages.itUpdatesAPetInTheDatabase, response = PetEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> put(HttpServletRequest request, @ApiParam(value = "ID of pet entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id, @ApiParam(value = "Pet entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) PetEntity entity) {
		return super.put(request, logger, service, id, entity);
	}

	@ApiOperation(value = APIMessages.itDeletesAPetInTheDatabase, response = ResponseEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> delete(HttpServletRequest request, @ApiParam(value = "ID of pet entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id, @ApiParam(value = "Pet entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) PetEntity entity) {
		return super.delete(request, logger, service, id, entity);
	}

}
