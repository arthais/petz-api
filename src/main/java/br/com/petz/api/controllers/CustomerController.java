package br.com.petz.api.controllers;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.api.entities.CustomerEntity;
import br.com.petz.api.entities.CustomerEntityList;
import br.com.petz.api.services.CustomerService;
import br.com.petz.api.structures.APIMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/customers")
@Api(tags = { "Customers" }, description = APIMessages.itRepresentsAllResourcesRelatedToTheCustomers)
public class CustomerController extends AbstractController<CustomerEntity> {

	Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerService service;

	@ApiOperation(value = APIMessages.itStoresACustomerInTheDatabase, response = CustomerEntity.class)
	@RequestMapping(value = { "" }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(HttpServletRequest request, @ApiParam(value = "Customer entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) CustomerEntity entity) {
		return super.post(request, logger, service, entity);
	}

	@ApiOperation(value = APIMessages.itProvidesAListOfAllCustomers, response = CustomerEntityList.class)
	@RequestMapping(value = { "" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAll(HttpServletRequest request) {
		return super.getAll(request, logger, service, new CustomerEntityList());
	}

	@ApiOperation(value = APIMessages.itProvidesACustomerIdentifiedByTheGivenId, response = CustomerEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getById(HttpServletRequest request, @ApiParam(value = "ID of customer entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id) {
		return super.getById(request, logger, service, id);
	}

	@ApiOperation(value = APIMessages.itUpdatesACustomerInTheDatabase, response = CustomerEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> put(HttpServletRequest request, @ApiParam(value = "ID of customer entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id, @ApiParam(value = "Customer entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) CustomerEntity entity) {
		return super.put(request, logger, service, id, entity);
	}

	@ApiOperation(value = APIMessages.itDeletesACustomerInTheDatabase, response = ResponseEntity.class)
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> delete(HttpServletRequest request, @ApiParam(value = "ID of customer entity", required = true) @PathVariable("id") @NotNull(message = APIMessages.itCannotBeNull) @DecimalMin(value = "1", message = APIMessages.itCannotBeLessThan) @DecimalMax(value = "9999999999999999999999999999", message = APIMessages.itCannotBeGreaterThan) BigDecimal id, @ApiParam(value = "Customer entity", required = true) @RequestBody(required = true) @NotNull(message = APIMessages.itCannotBeNull) CustomerEntity entity) {
		return super.delete(request, logger, service, id, entity);
	}

}
