package br.com.petz.api.controllers;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;

import br.com.petz.api.entities.AbstractEntityList;
import br.com.petz.api.exceptions.RepositoryDeleteException;
import br.com.petz.api.exceptions.RepositoryGetException;
import br.com.petz.api.exceptions.RepositoryNotFoundException;
import br.com.petz.api.exceptions.RepositoryPostException;
import br.com.petz.api.exceptions.RepositoryPutException;
import br.com.petz.api.exceptions.ServiceDeleteException;
import br.com.petz.api.exceptions.ServiceGetException;
import br.com.petz.api.exceptions.ServicePostException;
import br.com.petz.api.exceptions.ServicePutException;
import br.com.petz.api.exceptions.ServiceValidationException;
import br.com.petz.api.messages.ErrorMessage;
import br.com.petz.api.messages.SuccessMessage;
import br.com.petz.api.messages.WarningMessage;
import br.com.petz.api.services.AbstractService;
import br.com.petz.api.structures.APIMessages;
import br.com.petz.api.structures.APIResponse;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = {
		@ApiResponse(code = 200, message = APIMessages.therResourceSuccessfullyAccessed),
		@ApiResponse(code = 400, message = APIMessages.theRequestAppearsToBePoorlyFormatted),
		@ApiResponse(code = 401, message = APIMessages.youAreNotAuthorizedToAccessTheResource),
		@ApiResponse(code = 403, message = APIMessages.theResourceYouAreTryingToAccessIsProhibited),
		@ApiResponse(code = 404, message = APIMessages.theResourceYouWereTryingToAccessWasNotFound),
		@ApiResponse(code = 500, message = APIMessages.anUnexpectedSystemErrorHasOccurred)
})
@Validated
public class AbstractController<T> {

	// if there is any common code for all repositories, put it here.

	protected ResponseEntity<Object> post(HttpServletRequest request, Logger logger, AbstractService<T> service, T entity) {
		logger.info(MessageFormat.format(APIMessages.startingToPostTheResourceParameter + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI()));

		MultiValueMap<String, String> headers = new HttpHeaders();

		try {
			T body = service.post(entity);

			Method method = entity.getClass().getMethod("getId");
			BigDecimal id = (BigDecimal) method.invoke(body);

			String message = MessageFormat.format(APIMessages.theResourceSuccessfullyPostedParameter + " - id: " + id.toString(), request.getMethod() + " " + request.getRequestURI());
			APIResponse apiResponse = new APIResponse(new SuccessMessage(message), HttpStatus.CREATED, body);

			logger.info(message);

			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.CREATED);
		} catch (ServiceValidationException e) {
			String message = APIMessages.theResourceCouldNotBePosted + " " + e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI() + " - " + entity.toString();
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.BAD_REQUEST);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.BAD_REQUEST);
		} catch (RepositoryPostException | ServicePostException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBePostedParameter + " - " + e.getMessage() + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI());
			logger.error(message, e);
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	protected ResponseEntity<Object> getAll(HttpServletRequest request, Logger logger, AbstractService<T> service, AbstractEntityList<T> abstractEntityList) {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourcesParameter, request.getMethod() + " " + request.getRequestURI()));

		MultiValueMap<String, String> headers = new HttpHeaders();

		try {
			List<T> body = service.getAll();
			abstractEntityList.setEntityList(body);
			String message = MessageFormat.format(APIMessages.theResourcesSuccessfullyRecoveredParameter, request.getMethod() + " " + request.getRequestURI());
			APIResponse apiResponse = new APIResponse(new SuccessMessage(message), HttpStatus.OK, abstractEntityList);

			logger.info(message);

			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.OK);
		} catch (RepositoryNotFoundException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.NOT_FOUND);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.NOT_FOUND);
		} catch (RepositoryGetException | ServiceGetException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourcesCouldNotBeRecoveredParameter + " - " + e.getMessage(), request.getMethod() + " " + request.getRequestURI());
			logger.error(message, e);
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<Object> getById(HttpServletRequest request, Logger logger, AbstractService<T> service, BigDecimal id) {
		logger.info(MessageFormat.format(APIMessages.startingToRecoveryTheResourceParameter + " - id: " + id.toString(), request.getMethod() + " " + request.getRequestURI()));

		MultiValueMap<String, String> headers = new HttpHeaders();

		try {
			T body = service.getById(id);

			String message = MessageFormat.format(APIMessages.theResourceSuccessfullyRecoveredParameter + " - id: " + id.toString(), request.getMethod() + " " + request.getRequestURI());
			APIResponse apiResponse = new APIResponse(new SuccessMessage(message), HttpStatus.OK, body);

			logger.info(message);

			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.OK);
		} catch (RepositoryNotFoundException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.NOT_FOUND);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.NOT_FOUND);
		} catch (RepositoryGetException | ServiceGetException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeRecoveredParameter + " - id: " + id.toString() + " - " + e.getMessage(), request.getMethod() + " " + request.getRequestURI());
			logger.error(message, e);
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	protected ResponseEntity<Object> put(HttpServletRequest request, Logger logger, AbstractService<T> service, BigDecimal id, T entity) {
		logger.info(MessageFormat.format(APIMessages.startingToUpdateTheResourceParameter + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI()));

		MultiValueMap<String, String> headers = new HttpHeaders();

		try {
			Method method = entity.getClass().getMethod("getId");
			BigDecimal idTemp = (BigDecimal) method.invoke(entity);

			if (idTemp == null) {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.theValueOfXPropertyMustBeY, "entity.id", "]1..*["));
			}
			if (id.compareTo(idTemp) != 0) {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.thePathParamIdMustMatchTheEntityIdProperty, id, idTemp));
			}

			T body = service.put(entity);

			String message = MessageFormat.format(APIMessages.theResourceSuccessfullyUpdatedParameter + " - id: " + id.toString(), request.getMethod() + " " + request.getRequestURI());
			APIResponse apiResponse = new APIResponse(new SuccessMessage(message), HttpStatus.OK, body);

			logger.info(message);

			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.OK);
		} catch (RepositoryNotFoundException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.NOT_FOUND);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.NOT_FOUND);
		} catch (ServiceValidationException e) {
			String message = APIMessages.theResourceCouldNotBeUpdated + " " + e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.BAD_REQUEST);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.BAD_REQUEST);
		} catch (RepositoryPutException | ServicePutException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeUpdatedParameter + " - " + e.getMessage() + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI());
			logger.error(message, e);
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	protected ResponseEntity<Object> delete(HttpServletRequest request, Logger logger, AbstractService<T> service, BigDecimal id, T entity) {
		logger.info(MessageFormat.format(APIMessages.startingToDeleteTheResourceParameter + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI()));

		MultiValueMap<String, String> headers = new HttpHeaders();

		try {
			Method method = entity.getClass().getMethod("getId");
			BigDecimal idTemp = (BigDecimal) method.invoke(entity);

			if (idTemp == null) {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.theValueOfXPropertyMustBeY, "entity.id", "]1..*["));
			}
			if (id.compareTo(idTemp) != 0) {
				throw new ServiceValidationException(MessageFormat.format(APIMessages.thePathParamIdMustMatchTheEntityIdProperty, id, idTemp));
			}

			service.delete(entity);

			String message = MessageFormat.format(APIMessages.theResourceSuccessfullyDeletedParameter + " - id: " + id.toString(), request.getMethod() + " " + request.getRequestURI());
			APIResponse apiResponse = new APIResponse(new SuccessMessage(message), HttpStatus.NO_CONTENT);

			logger.info(message);

			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.OK);
		} catch (RepositoryNotFoundException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.NOT_FOUND);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.NOT_FOUND);
		} catch (ServiceValidationException e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeDeletedParameter + " - " + e.getMessage() + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI());
			logger.warn(message);
			APIResponse apiResponse = new APIResponse(new WarningMessage(message), HttpStatus.BAD_REQUEST);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.BAD_REQUEST);
		} catch (RepositoryDeleteException | ServiceDeleteException e) {
			String message = e.getMessage() + " Resource: " + request.getMethod() + " " + request.getRequestURI();
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			String message = MessageFormat.format(APIMessages.theResourceCouldNotBeDeletedParameter + " - " + e.getMessage() + " - " + entity.toString(), request.getMethod() + " " + request.getRequestURI());
			logger.error(message, e);
			APIResponse apiResponse = new APIResponse(new ErrorMessage(message), HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<Object>(apiResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
