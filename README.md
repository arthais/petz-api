No prompt do SO da sua máquina local configurar as variáveis de ambiente:

Windows:

set DB_HOST=107.180.12.35
set DB_USERNAME=taylor
set DB_PASSWORD=123456

Linux/bash:

export DB_HOST=107.180.12.35
export DB_USERNAME=taylor
export DB_PASSWORD=123456

Levantar a API:

java -jar petz-api-1.0.0-SNAPSHOT.jar

Após levantar a API o Swagger disponibiliza os end-points para teste e a auto documentação:

link: http://localhost:8080/petz/swagger-ui.html

Pode também ser acessado pelo SOAP-ui ou Postman.

Versão do Java: 1.8


